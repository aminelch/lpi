
## Reaching the command line interface 

- In a GUI mode, some distros provide a preinstalled graphical user interface (GUI) to interact with the shell. 

- Once you reach the prompt $, the Bash shell is waiting for you to enter commands.

- Some distros provide a C-Shell by default.

- The GUI includes a terminal emulator application for accessing the command-line interface.

- Outside the GUI, you need to reach what is called a virtual console.

- `Ctrl + Alt + F2` : Switch to the virtual terminal or virtual console.
- `Ctrl + Alt + F1` : Return to the GUI session.


