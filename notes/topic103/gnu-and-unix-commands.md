## Topic 103: GNU and Unix Commands


### history

```sh
$- history 20 #Display the last 20 commands
```

```sh
$- history -c #Clear the commands history list (only for current bash shell)
```

```sh
$- history -w #Overwrite history file with history of current bash shell
```

```sh
$- !9 #Refer to command line 9
```

```sh
$- !! #Refer to the previous command and execute it
```