### LPIC-1 prepation 

This repository contains some useful notes to prepare for the **LPIC-1** exam.


### Table of contents 

 - [Section 1: Introduction & Exploring the Basics of a Linux Environment](#https://gitlab.com/aminelch/lpi/-/tree/master/notes/section-1/index.md)


### Ressources 

- [LPI Learning Materials: LPIC-1 Exam 101](https://learning.lpi.org/en)
- [Linux Professional Institute LPIC-1 Exam 101 | KodeKloud](https://kodekloud.com/courses/linux-professional-institute-lpic-1-exam-101)
- [Vagrant by Hashicorp](https://www.vagrantup.com/)
- [CentOs Linux](https://www.centos.org/centos-linux/)
- [Ubuntu Linux](https://releases.ubuntu.com/18.04/)